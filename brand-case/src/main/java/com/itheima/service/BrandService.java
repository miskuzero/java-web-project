package com.itheima.service;

import com.itheima.pojo.Brand;

import java.util.List;

public interface BrandService {

    void add(Brand brand);

    void del(int id);

    void delete(int[] ids);

    void modify(Brand brand);

    Brand selectById(int id);

    int selectCountByConditions(Brand brand);

    List<Brand> selectPageByConditions(int begin, int pageSize, Brand brand);
}
