package com.itheima.web;

import com.alibaba.fastjson.JSON;
import com.itheima.pojo.Brand;
import com.itheima.pojo.PageBean;
import com.itheima.service.BrandService;
import com.itheima.service.impl.BrandServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/brand/*")
public class BrandHttpServlet extends BaseHttpServlet {
    BrandService brandService = new BrandServiceImpl();

    public void add(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String jsonString = request.getReader().readLine();
        Brand brand = JSON.parseObject(jsonString, Brand.class);
        brandService.add(brand);
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().print("ok");
    }

    public void del(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String _id = request.getParameter("id");
        int id = Integer.parseInt(_id);
        brandService.del(id);
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().print("ok");
    }

    public void delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String jsonString = request.getReader().readLine();
        int[] ids = JSON.parseObject(jsonString, int[].class);
        if (ids != null && ids.length > 0) {
            brandService.delete(ids);
            response.setContentType("text/json;charset=utf-8");
            response.getWriter().print("ok");
        }
    }

    //数据回显
    public void echo(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String i = request.getParameter("id");
        int id = Integer.parseInt(i);
        Brand brand = brandService.selectById(id);
        String jsonString = JSON.toJSONString(brand);
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().print(jsonString);
    }

    public void modify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String jsonString = request.getReader().readLine();
        Brand brand = JSON.parseObject(jsonString, Brand.class);
        brandService.modify(brand);
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().print("ok");
    }

    public void getPageByConditions(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String _currentPage = request.getParameter("currentPage");
        int currentPage = Integer.parseInt(_currentPage);
        String _pageNumber = request.getParameter("pageSize");
        int pageNumber = Integer.parseInt(_pageNumber);

        String _jsonString = request.getReader().readLine();
        Brand brand = JSON.parseObject(_jsonString, Brand.class);

        String companyName = "%" + brand.getCompanyName() + "%";
        String brandName = "%" + brand.getBrandName() + "%";

        brand.setBrandName(brandName);
        brand.setCompanyName(companyName);

        List<Brand> brands = brandService.selectPageByConditions((currentPage - 1) * pageNumber, pageNumber, brand);
        int count = brandService.selectCountByConditions(brand);

        PageBean<Brand> pageBean = new PageBean<>();
        pageBean.setTotalCount(count);
        pageBean.setCurrentList(brands);

        String jsonString = JSON.toJSONString(pageBean);
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().print(jsonString);
    }

}
