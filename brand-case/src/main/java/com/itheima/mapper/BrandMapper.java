package com.itheima.mapper;

import com.itheima.pojo.Brand;
import org.apache.ibatis.annotations.*;

import java.util.List;


public interface BrandMapper {

    @Insert("insert into tb_brand value (null,#{brandName},#{companyName},#{ordered},#{description},#{status})")
    void add(Brand brand);

    @Delete("delete from tb_brand where id = #{id}")
    void del(int id);

    void delete(@Param("ids") int[] ids);

    @Update("update tb_brand set brand_name = #{brandName},company_name = #{companyName},ordered = #{ordered},description = #{description},status = #{status} where id = #{id}")
    void modify(Brand brand);


    @Select("select * from tb_brand where id = #{id}")
    @ResultMap("brandResultMap")
    Brand selectById(int id);

    List<Brand> selectPageByConditions(@Param("begin") int begin, @Param("pageSize") int pageSize, @Param("brand") Brand brand);

    int selectCountByConditions(Brand brand);
}
