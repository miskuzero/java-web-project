package com.itheima.pojo;

import java.util.List;

public class PageBean<T> {
    private int totalCount;
    private List<T> currentList;

    public PageBean() {
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<T> getCurrentList() {
        return currentList;
    }

    public void setCurrentList(List<T> currentList) {
        this.currentList = currentList;
    }

    @Override
    public String toString() {
        return "PageBean{" +
                "totalCount=" + totalCount +
                ", currentList=" + currentList +
                '}';
    }
}
